﻿#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "common.h"

#pragma comment(linker, "/subsystem:\"windows\" /entry:\"mainCRTStartup\"")

int main()
{
    int version = GetD2Version("Game.exe");
#ifndef D2LOADER_RESTORE
    LPCSTR boxName = "Patch Storm.dll for D2Loader";
#else
    LPCSTR boxName = "Restore Storm.dll for D2Loader";
#endif

#if 0
    //不再检查版本号，统一使用v1.13c的storm.dll
    if ( V109d != version && V110f != version && V111b != version && V112a != version && V113c != version && V113d != version )
    {
        msgBox(boxName, MB_OK | MB_ICONEXCLAMATION,
            "Current version of LoD (%s) isn't compatible with D2Loader.\n\n"
            "Please, install a patch between 1.09d/1.10f/1.11b/1.12a/1.13c/1.13d.",
            GetVersionString(version));
        return 0;
    }
#else
    version = V113c;
#endif

#ifndef D2LOADER_RESTORE
    if ( msgBox(boxName, MB_YESNO | MB_ICONQUESTION,
            "This program will modify Storm.dll file in current directory.\n"
            "Before continue, you should backup it.\n\n"
            "Do you want to modify Storm.dll to install D2Loader(%s)?",  GetVersionString(version)) == IDNO  )
    {
        msgBox(boxName, MB_OK | MB_ICONASTERISK, "No changes made.");
        return 0;
    }
    else
    {
        FILE *targetFile;
        BYTE abPatch[] = {0xB8, 0x01, 0x00, 0x00, 0x00, 0xC2, 0x0C, 0x00}; //mov eax,0x1;retn 0xC;
        if ( fopen_s(&targetFile, "Storm.dll", "rb+") )
        {
            msgBox(boxName, MB_OK | MB_ICONEXCLAMATION,
                "Can't open Storm.dll in read/write mode.\n"
                "If Diablo II is running, can you close it and try again ?");
            return 0;
        }
        switch ( version )
        {
            case V113c:
                fseek(targetFile, 0x25CD6, SEEK_SET);   //6FC15CD6
                fwrite(abPatch, sizeof(abPatch), 1, targetFile);
                break;

            case V113d:            
                fseek(targetFile, 0x11EF6, SEEK_SET);   //6FC01EF6
                fwrite(abPatch, sizeof(abPatch), 1, targetFile);
                break;

            default:
                break;
        }
        fclose(targetFile);
        msgBox(boxName, MB_OK | MB_ICONASTERISK,
            "D2Loader, The D2Loader Kit already installed.");
    }
#else
    if ( msgBox(boxName, MB_YESNO | MB_ICONQUESTION,
            "This program will modify Storm.dll file in current directory.\n"
            "Before continue, you should backup it.\n\n"
            "Do you want to modify Storm.dll to uninsgall D2Loader(%s)?",  GetVersionString(version)) == IDNO  )
    {
        msgBox(boxName, MB_OK | MB_ICONASTERISK, "No changes made.");
        return 0;
    }
    else
    {
        FILE *targetFile;
        BYTE abPatch[] = {0x8B, 0x44, 0x24, 0x0C, 0x85, 0xC0, 0x75, 0x28}; //mov eax,dword ptr ss:[esp+0xC];test eax,eax;jnz short Storm.6FC01F26;
                                                                           //6FC15D06 1.13c
        if ( fopen_s(&targetFile, "Storm.dll", "rb+") )
        {
            msgBox(boxName, MB_OK | MB_ICONEXCLAMATION,
                "Can't open Storm.dll in read/write mode.\n"
                "If Diablo II is running, can you close it and try again ?");
            return 0;
        }
        switch ( version )
        {
            case V113c:
                fseek(targetFile, 0x25CD6, SEEK_SET);   //6FC15CD6
                fwrite(abPatch, sizeof(abPatch), 1, targetFile);
                break;

            case V113d:            
                fseek(targetFile, 0x11EF6, SEEK_SET);   //6FC01EF6
                fwrite(abPatch, sizeof(abPatch), 1, targetFile);
                break;

            default:
                break;
        }
        fclose(targetFile);
        msgBox(boxName, MB_OK | MB_ICONASTERISK,
            "D2Loader, The D2Loader Kit already uninstalled.");
    }
#endif

    return 0;
}

