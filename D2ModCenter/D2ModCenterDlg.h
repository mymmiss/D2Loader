﻿
// D2ModCenterDlg.h: 头文件
//
#include "..\common\D2LoaderVersion.h"

#pragma once

#define COLOR_DEFAULT 0 //默认颜色
#define COLOR_GREEN 1 //绿色
#define COLOR_BLUE  2 //蓝色

typedef struct
{
    DWORD expansion;
    BYTE window_mode;
    BYTE fix_aspect_ratio;
    BYTE glide_mode;
    BYTE opengl_mode;
    BYTE rave_mode;
    BYTE d3d_mode;
    BYTE perspective;
    BYTE low_quality;
    DWORD gamma;
    BYTE vsync;
    DWORD frame_rate;
    char acPadding6[4];
    WORD join_id;
    char game_name[24];
    char game_ip[24];
    char bnet_ip[24];
    char mcp_ip[24];
    DWORD unk1;
    BYTE no_pk;
    BYTE open_c;
    BYTE amazon;
    BYTE paladin;
    BYTE sorceress;
    BYTE necromancer;
    BYTE barbarian;
    BYTE dru;
    BYTE asn;
    BYTE invincible;
    char account_name[48];
    char player_name[24];
    char realm_name[27];
    char acPadding[0xFD];
    WORD c_temp;
    WORD char_flags;
    BYTE no_monsters;
    DWORD monster_class;
    BYTE monster_info;
    DWORD monster_debug;
    BYTE item_rare;
    BYTE item_unique;
    BYTE bPadding[2];
    DWORD act;
    BYTE no_preload;
    BYTE direct;
    BYTE low_end;
    BYTE no_gfx_compress;
    DWORD arena;
    char acPadding2[6];
    void* mpq_callback;
    BYTE txt;
    BYTE log;
    BYTE msg_log;
    BYTE safe_mode;
    BYTE no_save;
    DWORD seed;
    BYTE cheats;
    BYTE teen;
    BYTE no_sound;
    BYTE quests;
    BYTE unk4;
    BYTE build;
    BYTE sound_background;
    void* bnet_callbacks;
    char acPadding3[0x1C];
    char game_pass[24];
    char acPadding4[0x100];
    BYTE skip_to_bnet;
    BYTE unk5;
    char acPadding5[0x6B];
    WORD unk6;
} ST_CLIENT_DATA;

typedef struct
{
    ST_CLIENT_DATA stClientData;
    char acLanguageMpq[D2LOADER_MAXPATH];
    char acGameTitle[D2LOADER_MAXPATH];
    char acHackScriptPre[D2LOADER_MAXPATH];
    char acHackScript[D2LOADER_MAXPATH];
    char acModPath[D2LOADER_MAXPATH];
    char aacExtendMpq[MAX_EXTEND_MPQ][D2LOADER_MAXPATH];
    DWORD dwExtendMpq;
    char aacExtendPlugin[MAX_EXTEND_PLUGIN][D2LOADER_MAXPATH];
    DWORD dwExtendPlugin;
    char aacGlobalMpqPath[MAX_EXTEND_MPQ][D2LOADER_MAXPATH];
    DWORD dwGlobalMpqPath;
    char aacGlobalDllPath[MAX_EXTEND_PLUGIN][D2LOADER_MAXPATH];
    DWORD dwGlobalDllPath;
    char acDDrawPlugin[D2LOADER_MAXPATH];
    char acD2DxPlugin[D2LOADER_MAXPATH];
    BOOL boolDepFix;
    BOOL boolNoBorder;
    BOOL boolMultiOpen;
    BOOL boolTakePlugin;
    BOOL boolXpCompatible;
    BOOL boolWithConsole;
    BOOL boolNoHide;
    BOOL boolNoSleep;
    BOOL boolD2Common;
    BOOL boolShowAllDll;
} ST_MOD_PARAM;

// CD2ModCenterDlg 对话框
class CD2ModCenterDlg : public CDialogEx
{
// 构造
public:
    CD2ModCenterDlg(CWnd* pParent = nullptr);   // 标准构造函数

// 对话框数据
#ifdef AFX_DESIGN_TIME
    enum { IDD = IDD_D2MODCENTER_DIALOG };
#endif

protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV 支持

// 实现
protected:
    HICON m_hIcon;
    CToolTipCtrl m_ToolTip;//鼠标提示信息

    // 生成的消息映射函数
    virtual BOOL OnInitDialog();
    afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
    afx_msg void OnPaint();
    afx_msg HCURSOR OnQueryDragIcon();
    afx_msg void OnNMClickListmod(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnNMCustomdrawListmod(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnCbnSelchangeComboVer();
    afx_msg void OnCbnSelchangeComboVideo();
    afx_msg void OnCbnSelchangeComboLocale();
    afx_msg void OnBnClickedCheckwindow();
    afx_msg void OnEnChangeHackpre();
    afx_msg void OnEnChangeHack();
    afx_msg void OnEnChangeTitle();
    afx_msg void OnBnClickedCheckmultiopen();
    afx_msg void OnBnClickedCheckxp();
    afx_msg void OnBnClickedCheckdepfix();
    afx_msg void OnBnClickedCheckdirect();
    afx_msg void OnBnClickedChecktxt();
    afx_msg void OnBnClickedChecknosound();
    afx_msg void OnBnClickedChecknotitle();
    afx_msg void OnBnClickedChecknoborder();
    afx_msg void OnBnClickedCheckskiptobnet();
    afx_msg void OnBnClickedChecknohide();
    afx_msg void OnBnClickedChecknosleep();
    afx_msg void OnBnClickedCheckconsole();
    afx_msg void OnBnClickedCheckmpqpath();
    afx_msg void OnBnClickedCheckmpqfile();
    afx_msg void OnBnClickedCheckdllpath();
    afx_msg void OnBnClickedCheckdllfile();
    afx_msg void OnBnClickedCheckplugy();
    afx_msg void OnBnClickedBtndelmpqpath();
    afx_msg void OnBnClickedBtndeldllpath();
    afx_msg void OnBnClickedBtndelmpqfile();
    afx_msg void OnBnClickedBtndeldllfile();
    afx_msg void OnBnClickedBtnmpqpath();
    afx_msg void OnBnClickedBtndllpath();
    afx_msg void OnBnClickedBtnmpqfile();
    afx_msg void OnBnClickedBtndllfile();
    afx_msg void OnBnClickedBtnhelp();
    afx_msg void OnBnClickedBtnstart();
    afx_msg void OnTimer(UINT_PTR nIDEvent);
    virtual void OnOK();
    DECLARE_MESSAGE_MAP()

    void FindModInDir(CString rootDir, CString type, DWORD dwBkColor=COLOR_DEFAULT);
    void FindLocaleInDir(CString rootDir);
    void UpdateCmdParam();
    void resetClientData();
    void updateParamByDefault();
    BOOL UpdateD2LoaderVersion(LPCTSTR lpszFile);
    virtual BOOL PreTranslateMessage(MSG* pMsg);
    CStringA ParseMod(CString rootDir);
    CStringA ReadD2LoaderIni(CString filename);
    void parse_ini(CString filename);
    void parse_command(int argc, char** argv, ST_CLIENT_DATA *pstClientData);
    BOOL CopyDirectory(CString strSrcPath, CString strDestPath);
    BOOL DeleteFolder(LPCTSTR lpszPath);
    bool EnableDebugPrivilege();
    void CloseProcess(CString strExeName);
    void PatchBaseMod1133(CString strFileName);
    void PatchBaseMod1135(CString strFileName);
    void PatchBaseMod1136(CString strFileName);
    void PatchBaseModEntry(CString strFileName);
    void SelectOneMod(int iModIndex);
    BOOL checkVideoEnv();
    int readRegIntValue(HKEY hKey, LPCSTR lpSubKey, LPCSTR lpValueName, int defaultValue);
    int writeRegIntValue(HKEY hKey, LPCSTR lpSubKey, LPCSTR lpValueName, DWORD dwType, DWORD Data);
    int writeRegStringValue(HKEY hKey, LPCSTR lpSubKey, LPCSTR lpValueName, BYTE *lpData);

    CListCtrl m_ModList;
    DWORD m_dwModCount;

    CComboBox m_cbVersion;
    CComboBox m_cbVideoMode;
    CComboBox m_cbLocalePath;

    ST_MOD_PARAM m_stModParam;

    CStringA m_strVersion;
    CString m_strWorkingPath;
    CString m_strModPath;
    CString m_strModType;
    CString m_strCmdParam;
    CString m_strExtraParam;
    int m_iModIndex;
    BOOL m_boolWaitGameFinish;
    CString m_strTempLanguage;

public:
    afx_msg void OnNMRClickListmod(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnClistmenu32771();
    afx_msg void OnBnClickedBtnd2vidtst();
    afx_msg void OnBnClickedCheckcopyd2hd();
    afx_msg void OnBnClickedBtnhackpre();
    afx_msg void OnBnClickedBtnhack();
    afx_msg void OnBnClickedBtnkilld2();
    afx_msg void OnBnClickedBtnbasemod();
    afx_msg void OnClose();
    void setLocale(DWORD dwLocale);
    afx_msg void OnBnClickedCheckrestoredll();
    afx_msg void OnBnClickedCheckcommondll();
    afx_msg void OnBnClickedCheckalldll();
};

