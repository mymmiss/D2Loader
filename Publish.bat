@echo off
setlocal enabledelayedexpansion

set current_path=%cd%
set release_path="!current_path!\Test\D2ModCenter"

cd !release_path!
call :func

cd !release_path!\CORES
for /d %%i in (*) do (
    cd "%%i"
    call :func
    cd ..
)

cd !release_path!\MODS
for /d %%i in (*) do (
    cd "%%i"
    call :func
    cd ..
)

echo Zipping...
cd !current_path!\Test
..\7za.exe a ..\D2ModCenter.zip D2ModCenter -panders -y

copy /y !current_path!\Install\功能说明.txt !current_path!\Test\D2ModCenter\功能说明.txt
copy /y !current_path!\Install\D2Loader.exe !current_path!\Test\D2ModCenter\D2Loader.exe

cd !current_path!
echo Finish!
pause

:func
echo Processing %cd%...
rd /S /Q save>nul 2>nul
del /F /Q bncache.dat>nul 2>nul
del /F /Q D2Loader.ini>nul 2>nul
del /F /Q *.log>nul 2>nul
del /F /Q D*.txt>nul 2>nul
del /F /Q Crashdump>nul 2>nul
del /F /Q BnetLog.txt>nul 2>nul
del /F /Q D2CHAR.mpq>nul 2>nul
del /F /Q D2data.mpq>nul 2>nul
del /F /Q D2EXP.mpq>nul 2>nul
del /F /Q D2music.mpq>nul 2>nul
del /F /Q D2sfx.mpq>nul 2>nul
del /F /Q D2SPEECH.mpq>nul 2>nul
del /F /Q D2VIDEO.mpq>nul 2>nul
del /F /Q D2xmusic.mpq>nul 2>nul
del /F /Q D2XTALK.mpq>nul 2>nul
del /F /Q D2XVIDEO.mpq>nul 2>nul
goto :eof