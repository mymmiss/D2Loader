@echo off
%1 mshta vbscript:CreateObject("Shell.Application").ShellExecute("cmd.exe","/c %~s0 ::","","runas",1)(window.close)&&exit
cd /D %~dp0
setlocal enabledelayedexpansion
set current_path=%cd%
echo 开始测试，当前目录：%current_path%
echo 请开启D2ModCenter的测试模式。
reg add "HKCU\Software\MicROSoft\Windows NT\CurrentVersion\AppCompatFlags\Layers" /f /v \"%current_path%\..\D2ModCenter.exe\" /t REG_SZ /d "~ RunAsInvoker DisableNXShowUI WINXPSP3"
tasklist /FI "IMAGENAME eq D2ModCenter.exe"|find /i "D2ModCenter.exe">NUL 2>NUL
if "!ERRORLEVEL!" neq "0" (
    cd "%current_path%\.."&&start D2ModCenter.exe
)
cd "%current_path%"
echo ========================================
echo 请先对每个MOD修改BaseMod.dll，然后回车进入测试
pause
echo ========================================
set test_version=1.13c
set test_mode=ddraw
set test_type=CORES
if exist "%current_path%\%test_type%" (
    cd %test_type%
    for /d %%i in (*) do (
        if exist "%%i\D2Loader_ddraw.ini" (
            set test_version=%%i
            set test_mode=ddraw
            call :func
        )
        if exist "%%i\D2Loader_ddraw_plugin.ini" (
            set test_version=%%i
            set test_mode=ddraw_plugin
            call :func
        )
        if exist "%%i\D2Loader_glide.ini" (
            set test_version=%%i
            set test_mode=glide
            call :func
        )
        if exist "%%i\D2Loader_glide_plugin.ini" (
            set test_version=%%i
            set test_mode=glide_plugin
            call :func
        )
        if exist "%%i\D2Loader_cncddraw.ini" (
            set test_version=%%i
            set test_mode=cncddraw
            call :func
        )
        if exist "%%i\D2Loader_cncddraw_plugin.ini" (
            set test_version=%%i
            set test_mode=cncddraw_plugin
            call :func
        )
        if exist "%%i\D2Loader_d2dx.ini" (
            set test_version=%%i
            set test_mode=d2dx
            call :func
        )
        if exist "%%i\D2Loader_d2dx_plugin.ini" (
            set test_version=%%i
            set test_mode=d2dx_plugin
            call :func
        )
    )
    cd ..
)
set test_mode=ddraw
set test_type=MODS
if exist "%current_path%\%test_type%" (
    cd %test_type%
    for /d %%i in (*) do (
        if exist "%%i\D2Loader_ddraw.ini" (
            set test_version=%%i
            set test_mode=ddraw
            call :func
        )
        if exist "%%i\D2Loader_ddraw_plugin.ini" (
            set test_version=%%i
            set test_mode=ddraw_plugin
            call :func
        )
        if exist "%%i\D2Loader_glide.ini" (
            set test_version=%%i
            set test_mode=glide
            call :func
        )
        if exist "%%i\D2Loader_glide_plugin.ini" (
            set test_version=%%i
            set test_mode=glide_plugin
            call :func
        )
        if exist "%%i\D2Loader_cncddraw.ini" (
            set test_version=%%i
            set test_mode=cncddraw
            call :func
        )
        if exist "%%i\D2Loader_cncddraw_plugin.ini" (
            set test_version=%%i
            set test_mode=cncddraw_plugin
            call :func
        )
        if exist "%%i\D2Loader_d2dx.ini" (
            set test_version=%%i
            set test_mode=d2dx
            call :func
        )
        if exist "%%i\D2Loader_d2dx_plugin.ini" (
            set test_version=%%i
            set test_mode=d2dx_plugin
            call :func
        )
    )
    cd ..
)
echo 请手动测试大箱子修改存档路径的功能。
echo 1.14d的d2dx模式请取消XP兼容后，以及多开功能手动测试。
echo 请手动测试一下多人和战网模式是否正常。
echo 手动测试一下英文版界面。
echo 手动测试D2Loader_AllInOne.ini。
echo -locale和d2hack.script需要手动测试。
echo Finish！Press Enter to quit!
pause
exit

:func
echo ========================================
echo ========================================
echo testing "%test_type%" "%test_version%" "%test_mode%"
del /F /Q "%current_path%\..\%test_type%\%test_version%\test_result.txt">NUL 2>NUL
copy /y "%test_version%\D2Loader_%test_mode%.ini" "%current_path%\..\%test_type%\%test_version%\D2Loader.ini">NUL
echo>"%current_path%\..\%test_type%\%test_version%\test_start.txt"
echo %test_type%\%test_version%>"%current_path%\..\test_target.txt"
rem set /p test_param=<%test_version%\D2Loader_%test_mode%.ini
rem set src_str=..\..
rem set dst_str=%current_path%\..
rem set "test_param=!test_param:%src_str%=%dst_str%!"
rem echo "D2Loader.exe !test_param!"
rem cd %current_path%\..&&D2Loader.exe !test_param!
tasklist /FI "IMAGENAME eq D2ModCenter.exe"|find /i "D2ModCenter.exe">NUL 2>NUL
if "!ERRORLEVEL!" neq "0" (
    cd "%current_path%\.."&&start D2ModCenter.exe
)
:while
if not exist "%current_path%\..\%test_type%\%test_version%\test_result.txt" (
    ping -n 3 127.0.0.1>NUL 2>NUL
    goto :while
)
if exist "%current_path%\..\%test_type%\%test_version%\test_result.txt" (
    echo Result: 
    type "%current_path%\..\%test_type%\%test_version%\test_result.txt"
    echo ..
)
fc /c /l "%current_path%\%test_type%\%test_version%\D2Loader_%test_mode%.ini" "%current_path%\..\%test_type%\%test_version%\D2Loader.ini">NUL 2>NUL
if "!ERRORLEVEL!" neq "0" (
    echo "%current_path%\%test_type%\%test_version%\D2Loader_%test_mode%.ini"和"%current_path%\..\%test_type%\%test_version%\D2Loader.ini"有所不同，请手动比较后，按回车继续！
    pause
)
echo "%current_path%\..\%test_type%\%test_version%\Save"下面的存档包括：
for %%i in ("%current_path%\..\%test_type%\%test_version%\Save\*.d2s") do (
    echo %%~nxi，存档最近更新于：%%~ti
)
if exist "%current_path%\..\%test_type%\%test_version%\Save\Mod PlugY" (
    echo "%current_path%\..\%test_type%\%test_version%\Save\Mod PlugY"下面的存档包括：
    for %%i in ("%current_path%\..\%test_type%\%test_version%\Save\Mod PlugY\*.d2s") do (
        echo %%~nxi，存档最近更新于：%%~ti
    )
)
if exist "%current_path%\..\%test_type%\%test_version%\Save\Reign of Shadow" (
    echo "%current_path%\..\%test_type%\%test_version%\Save\Reign of Shadow"下面的存档包括：
    for %%i in ("%current_path%\..\%test_type%\%test_version%\Save\Reign of Shadow\*.d2s") do (
        echo %%~nxi，存档最近更新于：%%~ti
    )
)
if exist "%current_path%\..\Save" (
    echo "%current_path%\..\Save"下面的存档包括：
    for %%i in ("%current_path%\..\Save\*.d2s") do (
        echo %%~nxi，存档最近更新于：%%~ti
    )
)
del /F /Q "%current_path%\..\%test_type%\%test_version%\BaseMod.log">NUL 2>NUL
del /F /Q "%current_path%\..\%test_type%\%test_version%\PlugY.log">NUL 2>NUL
rem del /F /Q "%current_path%\..\%test_type%\%test_version%\D2Loader.ini">NUL 2>NUL
del /F /Q "%current_path%\..\%test_type%\%test_version%\test_start.txt">NUL 2>NUL
del /F /Q "%current_path%\..\%test_type%\%test_version%\test_result.txt">NUL 2>NUL
cd "%current_path%\%test_type%"
ping -n 10 127.0.0.1>NUL 2>NUL
goto :eof